**What is a  Docker?**
 
An open-source project that automates the deployment of software applications inside containers by providing an additional layer of abstraction and automation of OS-level virtualization on Linux.

**Block Diagram of a Docker Architecture**
![](https://devopedia.org/images/article/101/8323.1565281088.png)


**Some Docker Terminologies to remember**
_
- Docker
- Docker Image
- Container
- Docker Hub



**Docker commands**
```
docker ps :_The docker ps command allows us to view all the containers that are running on the Docker Host._
docker start:_This command starts any stopped container(s)._
docker stop:_This command stops any running container(s)_
docker run:_This command creates containers from docker images._
docker rm:_This command deletes the containers_
```
**Operations performed on  Dockers**

```
Download the docker. docker pull user_name/trydock
Run the docker image with this command docker run -ti user_name/trydock /bin/bash
Copy file inside the docker container docker cp hello.py e0b72ff850f8:/
All write a script for installing dependencies - requirements.sh apt update apt install python3
```
**Components of a Docker**

```
Docker Registry: A place docker images are stored. DockerHub is such a public registry.
Docker Engine: The place where containers and runs the programs.
Docker daemon: This place checks the client request and communicate with docker components such as image, container, to perform the
process.
Docker Client: It is the end user which provides the demands of the docker file as commands.
```

**Block diagram of Docker Components**
![](https://i.imgur.com/UWoeRAI.png)

**Why should we use Docker?**

For developers, there arises a situation where they need to work on different projects. And for each different project they might need a specific operating system, or a specific version of operating system. This becomes a tedious job to do. So problem can be solved using virtual machines, but this requires exact enviornment to run an application. Here we use Docker. The developer can work on the application and save it as docker image and create a docker container. This container has everything which is required to run an application alongwith the enviornment.This docker is then stored to repository and then can be used by other developers. So docker is used by many developers.

docker ps:_The docker ps command allows us to view all the containers that are running on the Docker Host._
docker start:_This command starts any stopped container(s)._
docker stop:_This command stops any running container(s)_
docker run:_This command creates containers from docker images._
docker rm:_This command deletes the containers_
Download the docker. docker pull user_name/trydock
Run the docker image with this command docker run -ti user_name/trydock /bin/bash
Copy file inside the docker container docker cp hello.py e0b72ff850f8:/
All write a script for installing dependencies - requirements.sh apt update apt install python3
Docker Registry: A place docker images are stored. DockerHub is such a public registry.
Docker Engine: The place where containers and runs the programs.
Docker daemon: This place checks the client request and communicate with docker components such as image, container, to perform the
process.
Docker Client: It is the end user which provides the demands of the docker file as commands.
