**What is Git ?**

Git is a version control (VCS) system for tracking changes to projects. Version control systems are also called revision control systems or source code management (SCM) systems it is used for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, nonlinear workflows.

![](https://verificationexcellence.in/wp-content/uploads/2018/07/reposetory.jpg)

**Getting started with Git**

_Instaling Git_

For Linux: sudo apt-get install git

For Windows: Download the git installer and run it

_first we should give git some basic details about us such as name and email address_

`git config --global user.name "Your Name Comes Here"`


`git config --global user.email your_email@yourdomain.example.com`

**Starting a new project on git**

In order to start a new project on git we use the linux command mkdir(make directory) to create a new directory to store data of our project using the command given below

`mkdir project_name`

In order to access a project that already exists we use the cd command where cd means change directory as shown below:

`cd project_name`

In order to delete a project file we use the linux command rmdir (remove directory) to delete the directory permanently

`rmdir project_name`

**Definition of Commands in GIT**


git status: check status and see what has changed

git add: add a changed file or a new file to be committed

git diff: see the changes between the current version of a file and the version of the file t recently committed

git commit: commit changes to the history

git log: show the history for a project

git revert: undo a change introduced by a specific commit

git checkout: switch branches or move within a branch

git clone: clone a remote repository

git pull: pull changes from a remote repoository

git push: push changes to remote repository

git checkout -b (branch name): create new branch

**Block Diagram of GIT workflow**

![](https://i.redd.it/nm1w0gnf2zh11.png)

**Understanding the workflow**

![](https://i.imgur.com/b0WypZh.png)

**What is the use of git?**

If you are working on a group assignment with some friends, and everyone has their own ideas.There are 2 possibilities to combine these ideas together:
- Person 1 writes his/her/their ideas on a single sheet of paper and passes it on to Person 2, who writes his/her/their ideas on the paper and passes it on to Person 3, etc.
- Everyone writes their ideas down on separate pieces of paper, and then consolidates them into a single sheet afterward.

****